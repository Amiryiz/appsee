package com.appsee.qa.testcases;

import com.appsee.qa.base.TestBase;
import com.appsee.qa.pages.HomePage;
import com.appsee.qa.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginPageTest extends TestBase {

    LoginPage loginPage;
    HomePage homepage;

    public  LoginPageTest(){
        super(); // Start the constructor in the TestBase class
    }

    @BeforeMethod
    public void setUp(){
        initialization();
        loginPage = new LoginPage();
    }

    @Test(priority = 1)
    public void loginPageTitleTest(){
        String title = loginPage.validateLoginPageTitle();
        Assert.assertEquals(title, "#1 Free");
    }

    @Test(priority = 2)
    public void crmLogoImageTest(){
        boolean flag = loginPage.validateCRMImage();
        Assert.assertTrue(flag);
    }

    @Test (priority = 3)
    public void validLoginTest(){
        homepage = loginPage.login(prop.getProperty("username"),prop.getProperty("password"));

    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

}
